all: manual.pdf man/wyrd.1 man/wyrd.7 man/wyrdrc.5 html/manual/index.html

page-manual.odoc: manual.mld
	odoc compile -I . --child page-wyrdrc --child page-wyrd $<

page-wyrd.odoc: wyrd.mld page-manual.odoc
	odoc compile -I . --parent page-manual $<

page-wyrdrc.odoc: wyrdrc.mld page-manual.odoc
	odoc compile -I . --parent page-manual $<

page-manual.odocl: page-manual.odoc page-wyrd.odoc page-wyrdrc.odocl
	odoc link -I . $<

page-wyrd.odocl: page-wyrd.odoc
	odoc link -I . $<

page-wyrdrc.odocl: page-wyrdrc.odoc
	odoc link -I . $<

html/manual/index.html: page-manual.odocl html/manual/wyrd.html html/manual/wyrdrc.html html/odoc.css
	odoc html-generate -o html $<

html/manual/wyrd.html: page-wyrd.odocl html/odoc.css
	odoc html-generate -o html $<

html/manual/wyrdrc.html: page-wyrdrc.odocl html/odoc.css
	odoc html-generate -o html $<

html/odoc.css:
	odoc support-files -o html

latex/manual.tex: page-manual.odocl
	odoc latex-generate -o latex $<
	sed -i'' -e '2,4 d' $@

latex/manual/wyrd.tex: page-wyrd.odocl
	odoc latex-generate -o latex $<

latex/manual/wyrdrc.tex: page-wyrdrc.odocl
	odoc latex-generate -o latex $<
	sed -i'' -e 's/page-manual-leaf-page-wyrdrc-//g' $@

manual.pdf: manual.tex latex/manual.tex latex/manual/wyrd.tex latex/manual/wyrdrc.tex
	latexmk -pdflua $<

man/wyrd.1: page-wyrd.odocl footer.man
	odoc man-generate -o man $<
	cat footer.man >> man/manual/wyrd.3o
	sed -e 's/manual.wyrd 3/wyrd 1/' -e 's/OCaml Library/a console calendar application/' \
	    -e 's/^manual..wyrd/wyrd - a text-based front-end to remind(1), a sophisticated calendar and alarm program./' \
	    -e '/.SH Documentation/d' -e 's/.in 3/.in 0/' \
	    -e 's/MANPAGES/wyrd(7), wyrdrc(5), remind(1)/' man/manual/wyrd.3o > $@
	rm man/manual/wyrd.3o
	rmdir man/manual || true

man/wyrd.7: page-manual.odocl footer.man
	odoc man-generate -o man $<
	cat footer.man >> man/manual.3o
	sed -e 's/manual 3/wyrd 7/' -e 's/OCaml Library/a console calendar application/' \
	    -e 's/^manual$$/wyrd - the user manual for wyrd(1), an ncurses interface to remind(1)./' \
	    -e 's/MANPAGES/wyrd(1), wyrdrc(5), remind(1)/' man/manual.3o > $@
	rm man/manual.3o

man/wyrdrc.5: page-wyrdrc.odocl footer.man
	odoc man-generate -o man $<
	cat footer.man >> man/manual/wyrdrc.3o
	sed -e 's/manual.wyrdrc 3/wyrdrc 5/' -e 's/OCaml Library/Wyrd configuration file/' \
	    -e 's/^manual..wyrdrc/wyrdrc - the configuration textfile for the wyrd(1) console calendar application./' \
	    -e 's/MANPAGES/wyrd(1), wyrd(7), remind(1)/' man/manual/wyrdrc.3o > $@
	rm man/manual/wyrdrc.3o
	rmdir man/manual || true

dist-clean:
	latexmk -c || true
	rm -Rvf page*.odoc* latex

clean: dist-clean
	rm -Rvf html man manual.pdf

.PHONY: all dist-clean clean
