#!/bin/sh

set -e

echo "Generating ./configure ..."
autoreconf -i && rm -rf autom4te.cache

# workaround autoconf < 2.71 not supporting -i
# project does not use automake so ignore error
if [ ! -f install-sh ]; then
	automake -a > /dev/null 2>&1 || true
	rm -rf autom4te.cache
fi
